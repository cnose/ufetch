# ufetch

Tiny system info for Unix-like operating systems.

![ufetch](https://jschx.gitlab.io/images/ufetch.png)

A fork of the original, pretty much exactly the same, except with experimental ChromeOS container support (ufetch-chromeos).
